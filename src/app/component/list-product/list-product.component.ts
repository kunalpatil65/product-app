import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { ProductService } from '../../service/product.service';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {

  public Product: any = [];
  p: number = 1;
  searchText;

  constructor(private productService: ProductService) {
    this.showProducts();
   }

  ngOnInit() {
  }

  showProducts() {
    this.productService.getAllProducts().subscribe(data => {
      this.Product = data;
    })
  }

  deleteProduct(id) {
    if (window.confirm('Are you sure?')) {
      this.productService.deleteProduct(id).subscribe((data) => {
        this.showProducts();
      })
    }
  }
}
