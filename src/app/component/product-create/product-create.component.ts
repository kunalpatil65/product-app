import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../../service/product.service';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';


@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {
  productForm : FormGroup;
  public categories: any[] = [];

  constructor(public formBuilder: FormBuilder, private router: Router, private productService: ProductService) {
    this.getCateroryList();
  }

  ngOnInit() {
    this.productForm = this.formBuilder.group({
      ProductName: ['', Validators.required],
      Description: ['', Validators.required],
      CategoryId: ['', Validators.required],
      Price: ['', Validators.required]
    })
  }

  getCateroryList() {
    this.productService.getAllCategories().subscribe(data => {
      Object.assign(this.categories, data);
    }, error => {
      console.log('Facing issue while loading categories')
    })
  }

  submitForm() {
    this.productService.createProduct(this.productForm.value).subscribe(data => {
      console.log('Product created successfully');
      this.router.navigateByUrl('/product-list');
    },
    error => {
      console.log(error);
    })
  }

}
