import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from '../../service/product.service';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {
  editProductForm: FormGroup;
  productId: number;
  product: any;

  constructor(public formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private productService: ProductService) {
  }

  ngOnInit() {
    this.getProduct(this.route.snapshot.params['id']);
    this.editProductForm = this.formBuilder.group({
      'ProductID': [this.route.snapshot.params['id']],
      'ProductName': ['', Validators.required],
      'Description': ['', Validators.required],
      'Price': ['', Validators.required]
    })
  }

  getProduct(id: any) {
    this.productService.getProduct(id).subscribe((data : any) => {
      this.editProductForm.setValue({
        ProductID: id,
        ProductName: data.ProductName,
        Description: data.Description,
        Price: data.Price
      });
    });
  }

  onSubmit() {
    this.productService.updateProduct(this.editProductForm.value).subscribe(data => {
      this.router.navigateByUrl('/product-list');
    },
    error => {
      console.log(error);
    })
  }

}
