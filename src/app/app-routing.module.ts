import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListProductComponent } from './component/list-product/list-product.component';
import { ProductEditComponent } from './component/product-edit/product-edit.component';
import { ProductCreateComponent } from './component/product-create/product-create.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'product-list' },
  { path: 'product-list', component: ListProductComponent },
  { path: 'create-product', component: ProductCreateComponent },
  { path: 'edit-product/:id', component: ProductEditComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
