import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  baseUri:string = 'http://localhost:50323/api';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  createProduct(data): Observable<any> {
    let API_URL = `${this.baseUri}/Product/Create`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.error)
      )
  }

  getProduct(id): Observable<any> {
    let url = `${this.baseUri}/Product/get/${id}`;
    return this.http.get(url, {headers: this.headers}).pipe(
      catchError(this.error)
      )
  }

  getAllProducts() {
    return this.http.get(`${this.baseUri}/Product/GetAll`);
  }

  updateProduct(data): Observable<any> {
    let API_URL = `${this.baseUri}/Product/Update`;
    return this.http.put(API_URL, data, { headers: this.headers }).pipe(
      catchError(this.error)
    )
  }

  deleteProduct(id): Observable<any> {
    var API_URL = `${this.baseUri}/Product/Delete/${id}`;
    return this.http.delete(API_URL).pipe(
      catchError(this.error)
    )
  }

  getAllCategories() {
    return this.http.get(`${this.baseUri}/Category/GetAll`);
  }

  error(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
